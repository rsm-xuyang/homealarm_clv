---
title: "Home Alarm CLV"
output: html_document
---

```{r r_setup, include = FALSE}
## initial settings
knitr::opts_chunk$set(
  comment = NA,
  echo = TRUE,
  error = TRUE,
  cache = FALSE,
  message = FALSE,
  dpi = 96,
  warning = FALSE
)

## width to use when printing tables etc.
options(
  width = 250,
  scipen = 100,
  max.print = 5000,
  stringsAsFactors = FALSE
)
```

<style>
.table {
  width: auto;
}
ul, ol {
  padding-left: 18px;
}
pre, code, pre code {
  overflow: auto;
  white-space: pre;
  word-wrap: normal;
  background-color: #ffffff;
}
</style>

Prepare "Home Alarm, Inc.: Assessing Customer Lifetime Value" for class discussion and as an individual assignment and submit the assignment through GitLab. Be VERY clear about where results are coming from and what assumptions you are making in your R code. It is in your best interest that we do not have to struggle to figure out where your numbers came from. The assignment (pdf) is in the class Dropbox folder (week2/homealarm-ltv.pdf). Example R and Excel calculations are also on Dropbox (week1/readings).

## Setup

Create an rmarkdown document in which you calculate the CLV for a customers who uses auto-pay and for a customer that does not use auto-pay and answer question 1 through 4 in the assignment PDF.

## Hints

This is an R Markdown document. Markdown is a simple formatting syntax for authoring HTML, PDF, and MS Word documents. For more details on using R Markdown see <http://rmarkdown.rstudio.com>. Go to http://commonmark.org/help/ for a 10-minute interactive Markdown tutorial

When you click the **Knit** button in Rstudio, a document will be generated that includes both the text you type as well as the output of any embedded R-code chunks within the document.

## Analysis

The code chunk below sets the attrition notifications you should use in your analysis. 

```{r}
## Attrition notifications 

churn <- tibble::tibble(
  no_autopay = c(0.084, 0.122, 0.162, 0.154, 0.134, 0.120,	0.111, 0.096, 0.086),
  autopay =  c(0.032, 0.070, 0.097, 0.103, 0.095, 0.078, 0.069, 0.059, 0.053)
)

### assumptions
## since the company is on year contract with customers, so we calculated the customers' retention rate on a yearly basis. What's more, the company is currently in the first year, and the charge of installation will incur at the beginning of that which means that the installation cost and charge will not be considered in the calculation below.
annual_growth <- 0.03
annual_discount_rate <- 0.1
annual_churn <- 0.2
cost_service <- 0.15
marketing_cost <- 0.05
nr_years <- 8
revenue <- 480
year <- 1:9



## without autopay
time <- 1:(nr_years)
growth <- rep((1 + annual_growth)^(0:(nr_years)))
revenues <- rep(revenue, nr_years+1) * growth
service <- cost_service * revenues 
marketing <- marketing_cost * revenues
profit <- revenues - service - marketing
active <- 1
for(i in 2:9){
  active[i] <- active[i-1]*(1-churn$no_autopay[i-1])
}

exp_profit <- active * profit

PV_exp_profit <- exp_profit / (1 + annual_discount_rate)^(year-1)
CLV <- sum(PV_exp_profit)
home <- tibble::tibble(year, PV_exp_profit)

radiant.data::visualize(home, xvar = "year", yvar = "PV_exp_profit", type = "line", custom = TRUE)



## with autopay

time2 <- 1:(nr_years)
growth2 <- rep((1 + annual_growth)^(0:(nr_years)))
revenues2 <- rep(revenue, nr_years+1) * growth2
service2 <- cost_service * revenues2 
marketing2 <- marketing_cost * revenues2
profit2 <- revenues2 - service2 - marketing2
active2 <- 1
for(i in 2:9){
  active2[i] <- active2[i-1]*(1-churn$autopay[i-1])
}



exp_profit2 <- active2 * profit2

PV_exp_profit2 <- exp_profit2 / (1 + annual_discount_rate)^(year-1)
CLV2 <-sum(PV_exp_profit2)
home2 <- tibble::tibble(year, PV_exp_profit2)
radiant.data::visualize(home2, xvar = "year", yvar = "PV_exp_profit2", type = "line", custom = TRUE)






## 3 the difference -maximum to pay 
## so the difference between the auto and non-auto pay are the amount that the company can charge which is 318 dollars
CLV2-CLV






### 4 marketing action
# first calculate how much must a churn rate decrease to make up the loss between this switch
churn_ra <- seq(0.12,0.01,by = -0.001)
CLV_ra <- c()
active3 <- 1
for(i in 1:length(churn_ra)){
  Attrition_non <- rep(churn_ra[i],9)
  Pro_Active_non <- c(active3)
  for(j in 2:9){
    Pro_Active_non[j] <- Pro_Active_non[j-1]*(1-Attrition_non[j-1])
  }
  E_Profit_non <-profit*Pro_Active_non
  PV_Profit_non <- E_Profit_non*(1/ (1 + annual_discount_rate)^(year-1))
  CLV_ra[i] <- sum(PV_Profit_non)
}

desired_churn_rate <- 0.12 - sum(CLV_ra <= CLV2)*0.001
desired_churn_rate
##The desired churn rate for nonauto-pay customers is 7.1%.

sensitive.df <- tibble::tibble(Churn_Ra <- churn_ra,nonauto_CLV <- CLV_ra, auto_CLV <- rep(CLV2,length(churn_ra)))
colnames(sensitive.df) <- c("Churn Rate","Nonauto CLV","Auto CLV")

## Sensitive analysis plot
library(tidyverse)
sensitive.df %>%
  gather(Pay,CLV,`Nonauto CLV`,`Auto CLV`) %>%
  ggplot(aes(`Churn Rate`, y = CLV, color = Pay))+geom_line()

# From the sensitive analysis result, we have calculated the churn rate 7.1% that will balance out the money between an nonauto user transfering to an auto_pay user based on the sensitivity analysis we conducted. And in order to decrease the churn rate here are the actions we provide: 

# Actions
# 1. To provide them an average discount for switching from non-auto pay to the auto pay. So we can decide the amount of the discount when charged the subscirption fee. So based on the calculation above we can calculate how much discount the company can provide to even out the CLV differences. after a couple of times of try out, the discount range should be between 0-01425, which means that it cannot give any discount higher than 14% off. if higher than 14%, then the company will start to loosing money

pv_revenue <- revenues *active *(1/(1 + annual_discount_rate)^(year-1))
sum(pv_revenue)
min_rev <- sum(pv_revenue)-318
r <- 0.1425
post_discount_rev <- revenues*(1-r)*active *(1/(1 + annual_discount_rate)^(year-1))
sum(post_discount_rev)
diff3 <- sum(post_discount_rev)-min_rev

# 2. the company can reduce or give discount to the stallation charge for the customers  who switch to the auto-pay as long as it does not exceed the 318 dollars limitation we calculated from the previous case, since the stallation will charge 195 dollars for all the new customers, so the marketing action should either be decrease these amount for people who switch from auto-pay to non-auto pay.

# 3. We can also use cash back or reduce the sales charge yearly for the customers who use auto-pay, and each month of the cash back could be the amount we saved and spread out to the amount of cash back we can provide. as calculated below that the reduce yearly basis should not exceed 73.8 USD.

pv_revenue2 <- revenues *active *(1/(1 + annual_discount_rate)^(year-1))
sum(pv_revenue2)
min_rev2 <- sum(pv_revenue2)-318
cash2 <- 73.8
post_discount_rev2 <- (revenues-cash2)*active *(1/(1 + annual_discount_rate)^(year-1))
sum(post_discount_rev2)
diff4 <- sum(post_discount_rev2)-min_rev2


# we can also suggest the company to transfer from annual contract to monthly in order to decrease the churn rate.


```

Please generate an HTML (Notebook) with your answers to all the questions listed in the homealarm-clv.pdf file on Dropbox. When you have finished editing the Rmarkdown document and generated the HTML report make sure to save, commit, and push it to GitLab. We will collect all the rmarkdown files from GitLab after the due date.


